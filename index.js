const rimraf = require("rimraf");
const git = require("simple-git/promise");

rimraf.sync(__dirname + "/data");

function gitClone() {
  git()
    .clone("git@bitbucket.org:gaurav_soni/manifests.git", __dirname + "/data", {
      recursive: true,
    })
    .then((res) => {
      console.log("Clone success", res);
    })
    .catch((err) => {
      console.log(err);
    });
}

function gitCommitPush() {
  git()
    .add("*")
    .then((res) => {
      console.log("Staged all the files");
      git()
        .commit("Deployment Server: Updated manifest files")
        .then((res) => {
          console.log("All files committed");
          git()
            .push("origin", "master")
            .then((res) => {
              console.log("Commit pushed to remote");
            })
            .catch((err) => {
              console.log("Error pushing commit to remote");
            });
        })
        .catch((err) => {
          console.log("Unable to commit the files");
        });
    })
    .catch((err) => {
      console.log("error staging the files", err);
    });
}

gitCommitPush();
